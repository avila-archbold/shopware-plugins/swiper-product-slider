<?php declare(strict_types=1);

namespace FaaSlider\DataResolver;

use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\Element\AbstractCmsElementResolver;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Cms\DataResolver\CriteriaCollection;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Struct\ArrayStruct;

class ExtSliderCmsElementResolver extends AbstractCmsElementResolver
{
    public function getType(): string
    {
        return 'ext-slider';
    }

    public function collect(CmsSlotEntity $slot, ResolverContext $resolverContext): ?CriteriaCollection
    {
        $productIdConfig = $slot->getFieldConfig()->get('products');

        if ($productIdConfig === null) {
            return null;
        }

        $productIds = $productIdConfig->getValue();

        $criteria = new Criteria($productIds);
        $criteria->addAssociation('cover'); // Load the cover image association
    
        $criteriaCollection = new CriteriaCollection();  
        $criteriaCollection->add('product_' . $slot->getUniqueIdentifier(), ProductDefinition::class, $criteria);
    
        return $criteriaCollection;
    }

    public function enrich(CmsSlotEntity $slot, ResolverContext $resolverContext, ElementDataCollection $result): void
    {
        $productSearchResult = $result->get('product_' . $slot->getUniqueIdentifier());
    
        if ($productSearchResult === null) {
            return;
        }
    
        $slot->setData(new ArrayStruct([
            'products' => $productSearchResult->getEntities(),
        ]));
    }
}
