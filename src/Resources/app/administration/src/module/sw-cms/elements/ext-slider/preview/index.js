import template from './sw-cms-el-preview-ext-slider.html.twig';
import './sw-cms-el-preview-ext-slider.scss';

/**
 * @private
 * @package buyers-experience
 */
export default {
    template,
};
