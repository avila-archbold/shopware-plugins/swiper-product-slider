import template from './sw-cms-el-ext-slider.html.twig';
import './sw-cms-el-ext-slider.scss';

const { Mixin, Component } = Shopware;
const { mapState } = Component.getComponentHelper();

/**
 * @private
 * @package buyers-experience
 */
export default {
    template,

    inject: ['feature'],

    mixins: [
        Mixin.getByName('cms-element'),
    ],

    data() {
        return {
            sliderBoxLimit: 3,
        };
    },

    computed: {
        ...mapState('cmsPageState', ['currentCmsElement']),

        demoProductElement() {
            return {
                config: {
                    boxLayout: {
                        source: 'static',
                        value: this.element.config.boxLayout.value,
                    },
                    displayMode: {
                        source: 'static',
                        value: this.element.config.displayMode.value,
                    },
                },
                data: null,
            };
        },
        sliderConfig() {
            return this.element.config;
        },
        classes() {
            return;
        },
        currentDeviceView() {
            return this.cmsPageState.currentCmsDeviceView;
        },
    },

    watch: {
        currentDeviceView() {
            setTimeout(() => {
                this.setSliderRowLimit();
            }, 400);
        },
    },

    created() {
        this.createdComponent();
    },

    mounted() {
        this.mountedComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('ext-slider');
            this.initElementData('ext-slider');
        },

        mountedComponent() {
            this.setSliderRowLimit();
        },

        setSliderRowLimit() {
            this.sliderBoxLimit = 3;
            return;
        },

        getProductEl(product) {
            return {
                config: {
                    boxLayout: {
                        source: 'static',
                        value: this.element.config.boxLayout.value,
                    },
                    displayMode: {
                        source: 'static',
                        value: this.element.config.displayMode.value,
                    },
                },
                data: {
                    product,
                },
            };
        },

        onElementUpdate(element) {
            this.$emit('element-update', element);
        }
    },
};
