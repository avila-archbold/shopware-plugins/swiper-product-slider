/**
 * @private
 * @package buyers-experience
 */
Shopware.Component.register('sw-cms-el-preview-ext-slider', () => import('./preview'));
/**
 * @private
 * @package buyers-experience
 */
Shopware.Component.register('sw-cms-el-config-ext-slider', () => import('./config'));
/**
 * @private
 * @package buyers-experience
 */
Shopware.Component.register('sw-cms-el-ext-slider', () => import('./component'));

const Criteria = Shopware.Data.Criteria;
const criteria = new Criteria(1, 25);
criteria.addAssociation('cover');

Shopware.Service('cmsService').registerCmsElement({
    name: 'ext-slider',
    label: 'sw-cms.elements.extSlider.label',
    component: 'sw-cms-el-ext-slider',
    configComponent: 'sw-cms-el-config-ext-slider',
    previewComponent: 'sw-cms-el-preview-ext-slider',
    defaultConfig: {
        products: {
            source: 'static',
            value: [],
            required: true,
            entity: {
                name: 'product',
                criteria: criteria,
            },
        },
        title: {
            source: 'static',
            value: '',
        },
        displayMode: {
            source: 'static',
            value: 'standard',
        },
        boxLayout: {
            source: 'static',
            value: 'standard',
        },
        productStreamSorting: {
            source: 'static',
            value: 'name:ASC',
        },
        productStreamLimit: {
            source: 'static',
            value: 10,
        },
        slidesPerView: {
            source: 'static',
            value: 1,
        },
        loop: {
            source: 'static',
            value: true,
        },        autoplay: {
            source: 'static',
            value: false,
        },
        autoplayDelay: {
            source: 'static',
            value: 5000,
        },
        effect: {
            source: 'static',
            value: 'slide',
        },
    },
    collect: Shopware.Service('cmsService').getCollectFunction(),
});
