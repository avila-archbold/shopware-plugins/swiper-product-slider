/**
 * @private
 * @package buyers-experience
 */
Shopware.Component.register('sw-cms-preview-ext-slider', () => import('./preview'));
/**
 * @private
 * @package buyers-experience
 */
Shopware.Component.register('sw-cms-block-ext-slider', () => import('./component'));

/**
 * @private
 * @package buyers-experience
 */
Shopware.Service('cmsService').registerCmsBlock({
    name: 'ext-slider',
    label: 'sw-cms.blocks.commerce.extSlider.label',
    category: 'commerce',
    component: 'sw-cms-block-ext-slider',
    previewComponent: 'sw-cms-preview-ext-slider',
    defaultConfig: {
        marginBottom: '0px',
        marginTop: '0px',
        marginLeft: '0px',
        marginRight: '0px',
        sizingMode: 'boxed',
    },
    slots: {
        extSlider: 'ext-slider',
    },
});
