import template from './sw-cms-block-ext-slider.html.twig';

/**
 * @private
 * @package buyers-experience
 */
export default {
    template,
};
