import template from './sw-cms-preview-ext-slider.html.twig';
import './sw-cms-preview-ext-slider.scss';

/**
 * @private
 * @package buyers-experience
 */
export default {
    template,
};
