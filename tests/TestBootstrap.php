<?php

use Shopware\Core\TestBootstrapper;

$classLoader = require dirname(__DIR__, 4) . '/vendor/autoload.php';

(new TestBootstrapper())
    ->setClassLoader($classLoader)
    ->setProjectDir(dirname(__DIR__, 4))
    ->setLoadEnvFile(true)
    ->bootstrap();
