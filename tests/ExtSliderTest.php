<?php

namespace Vendor\FaaSlider\Tests;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\CmsElement\CmsElementRegistry;
use Shopware\Core\Framework\Test\TestCaseBase\IntegrationTestBehaviour;

class ExtSliderTest extends TestCase
{
    use IntegrationTestBehaviour;

    public function testPluginIsRegistered(): void
    {
        $elementRegistry = $this->getContainer()->get(CmsElementRegistry::class);
        $element = $elementRegistry->get('ext-slider');

        $this->assertNotNull($element);
    }

    public function testSliderConfigHasDefaultValues(): void
    {
        $elementRegistry = $this->getContainer()->get(CmsElementRegistry::class);
        $element = $elementRegistry->get('ext-slider');

        $defaultConfig = $element->getDefaultConfig();

        $this->assertArrayHasKey('slidesPerView', $defaultConfig);
        $this->assertEquals(1, $defaultConfig['slidesPerView']['value']);

        $this->assertArrayHasKey('loop', $defaultConfig);
        $this->assertFalse($defaultConfig['loop']['value']);

        $this->assertArrayHasKey('autoplay', $defaultConfig);
        $this->assertFalse($defaultConfig['autoplay']['value']);

        $this->assertArrayHasKey('autoplayDelay', $defaultConfig);
        $this->assertEquals(1000, $defaultConfig['autoplayDelay']['value']);

        $this->assertArrayHasKey('effect', $defaultConfig);
        $this->assertEquals('slide', $defaultConfig['effect']['value']);
    }
}
